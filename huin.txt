{
    "_id": "580dfe09f0fc145cccae834c",
    "index": 2,
    "guid": "e48d6d05-dc40-4996-88c9-655792718fd6",
    "isActive": false,
    "balance": "$1,148.33",
    "picture": "http://placehold.it/32x32",
    "age": 23,
    "eyeColor": "brown",
    "name": "Harriet Kline",
    "gender": "male",
    "company": "JUNIPOOR",
    "email": "harrietkline@junipoor.com",
    "phone": "+1 (947) 508-2358",
    "address": "961 Lloyd Court, Norris, American Samoa, 5787",
    "about": "Mollit in mollit eiusmod aute magna aute. Fugiat fugiat veniam sint qui amet consectetur velit nostrud ea. Dolor aute nostrud minim sit aute aliqua dolor eu culpa ullamco.\r\n",
    "registered": "2016-05-11T11:08:05 -03:00",
    "latitude": 42.741161,
    "longitude": 179.263676,
    "tags": [
      "commodo",
      "veniam",
      "aliqua",
      "fugiat",
      "cupidatat",
      "pariatur",
      "excepteur"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Twila Norris"
      },
      {
        "id": 1,
        "name": "Elliott Estes"
      },
      {
        "id": 2,
        "name": "Langley Booth"
      },
      {
      	"id":3,
      }
    ],
    "greeting": "Hello, Harriet Kline! You have 9 unread messages.",
    "favoriteFruit": "strawberry"
  }
